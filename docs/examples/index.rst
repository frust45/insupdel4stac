..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0

.. _examples:


======================
Examples of use
======================
In this section, we provide a few examples of use of ``INSUPDEL4STAC``. We listed them all in below:


.. toctree::

   INSUPDEL4STAC.ipynb
